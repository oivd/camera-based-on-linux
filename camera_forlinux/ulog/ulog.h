/**************************************************************************
@file: ulog.h
@data: 2023.01.10
@author: 96oivd

ulog    微型的日志调试工具
支持日志记录级别设置
G_DEBUG -- 调试级别，记录非常详细的步骤信息
G_TRACE -- 跟踪级别，记录重要的跟踪信息
G_WARNG -- 警告级别，记录警告信息
G_ERROR -- 错误级别，记录错误信息
ulog支持基于宏和函数的两种实现方式

@修订说明:

****************************************************************************/

#ifndef _U_LOG_H_
#define _U_LOG_H_

// **************************************************************************
// include
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

// **************************************************************************
// 用户配置

// **************************************************************************
// 宏定义

//日志记录级别
#define	DEBUG (0)     /* 信息用来显示详细的执行信息和步骤 */
#define	TRACE (1)     /* 用来跟踪特定的主要信息 */
#define INFO  (2)     /* 普通信息 */
#define WARN  (3)     /* 警告级别 */
#define	ERROR (4)     /* 错误级别 */
#define	NONE  (5)     /* 关闭LOG */

// log级别
#define LOG_CFG_LEVEL   (DEBUG)
// 字符输出函数
#define LOG_PUT_CHAR(C) putchar(C)
// 日志记录接口
#define LOG_PRINTF(format, ...) printf(format, ##__VA_ARGS__)

// **************************************************************************
// 接口定义

//兼容C C++混合编程
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

void log_printmem(void* buff, uint32_t size, uint32_t addr, uint8_t linsize);
static inline char *get_str_clock(){
    struct timeval tv;
    static char str_clock[36];
    memset(str_clock, 0, sizeof(str_clock));
    gettimeofday(&tv, NULL);
    struct tm *tm = localtime(&tv.tv_sec);
    snprintf(str_clock, sizeof(str_clock), 
            "%04d-%02d-%02d %02d:%02d:%02d.%03ld", 
            tm->tm_year+1900, tm->tm_mon+1, 
            tm->tm_mday, tm->tm_hour, 
            tm->tm_min, tm->tm_sec, tv.tv_usec / 1000);

    return str_clock;
}

#ifdef __cplusplus
}
#endif  /*__cplusplus*/

// **************************************************************************
// 定义打印样式
#if 0
#define LOG_FUN(LEVESTR,format, ...)       \
   LOG_PRINTF("["LEVESTR"] <"__FILE__": %d"">: " format "\r\n",__LINE__,##__VA_ARGS__)
#else
#define LOG_FUN(LEVESTR,format, ...)                    \
        LOG_PRINTF("[%s]["LEVESTR"][%s %d]: "           \
                    format, get_str_clock(),            \
                    __func__, __LINE__, ##__VA_ARGS__)
#endif

// #define LOG_FUN_HEAD(LEVESTR,format, ...)
//    LOG_PRINTF("\r["LEVESTR"][%s %d""]: " format ,__func__,__LINE__,##__VA_ARGS__)

// **************************************************************************

#if LOG_CFG_LEVEL>=NONE
/* 关闭LOG */
#define uLOG(LEVE,format, ...)   /*(0)*/
#define uLOG_MEM(aLEVE,ADDRESS,LEN) (0)
#define uLOG_HEAD(LEVE,format, ...)
#else
/* 使用宏实现 */
#define uLOG(aLEVE,format, ...)         \
    do {                                \
        if (aLEVE>=LOG_CFG_LEVEL) {     \
            LOG_FUN(#aLEVE,format,##__VA_ARGS__); \
        }                               \
    } while(0)

#define uLOG_MEM(aLEVE,ADDRESS,LEN,format, ...) \
    do {                                        \
        if (aLEVE>=LOG_CFG_LEVEL) {             \
            LOG_FUN(#aLEVE,format,##__VA_ARGS__); \
            log_printmem(ADDRESS,LEN,0,16);     \
        }   \
    } while(0)
#endif  /* LOG_CFG_LEVEL>=NONE */

#define ulog_debug(fmt, ...)    uLOG(DEBUG, fmt, ##__VA_ARGS__)
#define ulog_info(fmt, ...)     uLOG(INFO, fmt, ##__VA_ARGS__)
#define ulog_warn(fmt, ...)     uLOG(WARN, fmt, ##__VA_ARGS__)
#define ulog_error(fmt, ...)    uLOG(ERROR, fmt, ##__VA_ARGS__)

// #define ulog_debug_head(fmt, ...)    uLOG_HEAD(G_DEBUG, fmt, ##__VA_ARGS__)
// #define ulog_info_head(fmt, ...)     uLOG_HEAD(G_INFO, fmt, ##__VA_ARGS__)
// #define ulog_warn_head(fmt, ...)     uLOG_HEAD(G_WARN, fmt, ##__VA_ARGS__)
// #define ulog_error_head(fmt, ...)    uLOG_HEAD(G_ERROR, fmt, ##__VA_ARGS__)

#endif  /* _U_LOG_H_ */
