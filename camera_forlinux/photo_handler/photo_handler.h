#ifndef _PHOTO_HANDLER_H
#define _PHOTO_HANDLER_H

#include "list/list.h"

struct photo_handler {
    list_t *list;

    void *photo_data;
    long used_bytes;
    long total_bytes;
};

struct photo_handler *photo_init(void);

void photo_destroy(struct photo_handler *ph);

int photo_append(struct photo_handler *ph, const char *name, void *data, long size);

void photo_remove(struct photo_handler *ph, const char *name);

int photo_rgetdata(struct photo_handler *ph, void **data, long *size);
int photo_lgetdata(struct photo_handler *ph, void **data, long *size);
int photo_get_currentdata(struct photo_handler *ph, void **data, long *size);

#endif /* _PHOTO_HANDLER_H */