#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "photo_handler.h"
#include "ulog.h"

static void photo_free(void *val)
{
    free(val);
} 

static int photo_match(void *a, void *b)
{
    return (strcmp(a, b) == 0) ? 1 : 0;
}

struct photo_handler *photo_init(void)
{
    struct photo_handler *ph = malloc(sizeof(struct photo_handler));
    if (!ph) 
        return NULL;
    
    ph->list = list_new();
    if (!ph->list)
        goto err;

    ph->list->free = photo_free;
    ph->list->match = photo_match;

    ph->photo_data = NULL;
    ph->used_bytes = 0;
    ph->total_bytes = 0;

    return ph;
err:
    free(ph);
    return NULL;
}

void photo_destroy(struct photo_handler *ph)
{
    list_destroy(ph->list);
    ph->list = NULL;

    free(ph->photo_data);
    ph->photo_data = NULL;

    free(ph);
}

int 
photo_append(struct photo_handler *ph, const char *name, void *data, long size)
{
    int fd = open(name, O_CREAT | O_RDWR | O_TRUNC, 0666);
    if (fd < 0) {
        ulog_warn("open failed:%s", strerror(errno));
        return -1;
    }

    if (data != NULL) {
        int ret = write(fd, data, size);
        // ulog_debug("write ret:%d, size:%ld\n", ret, size);
        if (ret < size) {
            ulog_warn("write failed:%s", strerror(errno));
            return -1;
        }
    }
    close(fd);

    if (list_rpush(ph->list, list_node_new(strdup(name))) == NULL) {
        ulog_warn("list_rpush failed!\n");
        return -1;
    }

    return 0;
}

void photo_remove(struct photo_handler *ph, const char *name)
{
    list_remove(ph->list, list_find(ph->list, (void *)name));
}

static int 
photo_getdata(struct photo_handler *ph, void **data, long *size, int dire)
{
    static int index = 0;

    if (ph->list->len == 0)
        return -1;

    if (dire) {
        if (++index >= ph->list->len)
            index = 0;
    } else {
        if (--index < 0)
            index = ph->list->len - 1;
    }

    int ret = 0;
    list_node_t *pnode = list_at(ph->list, index);
    if (pnode == NULL) {
        ulog_warn("no photoes!\n");
        ret = -1;
        goto err;
    }

    char *img_name = (char *)pnode->val;
    ulog_debug("%s\n", (char *)pnode->val);
    int fd = open(img_name, O_RDONLY);
    if (fd < 0)
        return -1;
    
    struct stat stat_buf;
    if (fstat(fd, &stat_buf) < 0) {
        ret = -1;
        goto err;
    }

    if (ph->photo_data == NULL) {
        ph->photo_data = malloc(stat_buf.st_size);
        if (!ph->photo_data) {
            ret = -1;
            goto err;
        }
        ph->total_bytes = stat_buf.st_size;
    }

    if (ph->total_bytes < stat_buf.st_size) {
        void *pnew = realloc(ph->photo_data, stat_buf.st_size);
        if (!pnew) {
            ulog_warn("realloc failed!\n");
        } else {
            ph->photo_data = pnew;
            ph->total_bytes = stat_buf.st_size;
        }
    }
        
    ret = read(fd, ph->photo_data, stat_buf.st_size);
    if (ret < 0) {
        close(fd);
        return -1;
    }
    ph->used_bytes = ret;

    *data = ph->photo_data;
    *size = ph->used_bytes;

    close(fd);
    return 0;
err:
    *data = NULL;
    *size = 0;
    close(fd);
    return ret;
}

int photo_rgetdata(struct photo_handler *ph, void **data, long *size)
{
    return photo_getdata(ph, data, size, 1);
}

int photo_lgetdata(struct photo_handler *ph, void **data, long *size)
{
    return photo_getdata(ph, data, size, 0);
}

int photo_get_currentdata(struct photo_handler *ph, void **data, long *size)
{
    *data = ph->photo_data;
    *size = ph->used_bytes;
    return 0;
}