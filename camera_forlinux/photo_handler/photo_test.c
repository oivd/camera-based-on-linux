#include <stdio.h>
#include <string.h>

#include "photo_handler.h"

#if 0
int main()
{
    struct photo_handler *ph = photo_init();
    if (!ph)
        return -1;

    photo_append(ph, "abcd");
    photo_append(ph, "qwert");
    photo_append(ph, "12345");

    for (int i = 0; i < ph->list->len; i++) {
        printf("%s  ", (char *)list_at(ph->list, i)->val);
    }
    printf("\n");

    photo_remove(ph, "12345");
    for (int i = 0; i < ph->list->len; i++) {
        printf("%s  ", (char *)list_at(ph->list, i)->val);
    }
    printf("\n");

    photo_destroy(ph);
    return 0;
}
#else
int main()
{
    struct photo_handler *ph = photo_init();
    if (!ph)
        return -1;

    photo_append(ph, "debug.log", NULL, 0);
    photo_append(ph, "photo_handler.c", NULL, 0);

    for (int i = 0; i < ph->list->len; i++) {
        printf("%s  ", (char *)list_at(ph->list, i)->val);
    }
    printf("\n");

    char data[3*1024];
    long data_size;

    for (int i = 0; i < 2; i++) {
        memset(data, 0, sizeof(data));
        if (photo_getdata(ph, &data, &data_size) < 0) {
            printf("getdata failed!\n");
            goto err;
        }
        printf("data_size:%ld\n\n%s\n", data_size, data);
    }

err:
    photo_destroy(ph);
    return 0;
}

#endif