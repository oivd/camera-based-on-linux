#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/videodev2.h>
#include <pthread.h>

#include "display_manager.h"
#include "display_framebuffer.h"
#include "video_controller.h"
#include "image_handler.h"
#include "photo_handler.h"
#include "mouse_controller.h"
#include "ulog.h"

#define FB_PATH "/dev/fb0"
#define PHOTO_NAME "photo_image"

static volatile int mouse_val;

static void *get_mouse_val(void *mouse_fd)
{
    int val; 

    for (;;) {
        val = usbmouse_get_val(*(int *)mouse_fd);

        if (val < MOUSE_UNKNOW)
            break;
        if (val != MOUSE_UNKNOW) {
            ulog_debug("usbmouse_get_val:%d\n", val);
            mouse_val = val;
        }
    }

    pthread_exit(NULL);
}

int main(int argc, char **argv)
{
	// if (argc != 2) {
    //     printf("Usage: %s </dev/videoX>\n", argv[0]);
    //     return -1;
    // }

    /* 注册 */
    struct display_device disp_dev;
    if (dispfb_register(&disp_dev) < 0)
        return -1;

    /* 初始化工作 */
	struct video_device *video_devs;
    struct video_device video_dev;
    int video_num = 0;
    int video_index = 0;

    video_num = vctl_init_devices(&video_devs);
    ulog_info("video_num:%d\n", video_num);
    if (video_num <= 0)
        return -1;
    video_dev = video_devs[0];

    disp_dev.ops->init(FB_PATH, &disp_dev);

    struct image_handler *img_handle;
    imghdl_init();
    img_handle = imghdl_get_handler_fromfmt
                (V4L2_PIX_FMT_RGB24, video_dev.pixel_format);
    if (img_handle == NULL) {
        ulog_warn("img_handle get failed\n");
    }

    struct photo_handler *ph_handle;
    ph_handle = photo_init();

    int mouse_fd = usbmouse_open();
    if (mouse_fd < 0) {
        ulog_warn("mouse no found\n");
    }

    pthread_t tid;
    pthread_create(&tid, NULL, get_mouse_val, &mouse_fd);

    /* initialized end */

    /* 开始数据传输 */
	video_dev.vi_ops->start_dev(&video_dev);

    struct frame_datas convert_data;
    memset(&convert_data, 0, sizeof(struct frame_datas));
    convert_data.bpp = disp_dev.bpp;

    int lcd_width = disp_dev.x_res;
    int lcd_height = disp_dev.y_res;
    int lcd_bpp = disp_dev.bpp;

    struct frame_datas zoom_data;
    memset(&zoom_data, 0, sizeof(zoom_data));

	struct frame_datas raw_frame, video_frame;
    struct frame_datas *lcd_data;
    struct frame_datas photo_data;
    memset(&photo_data, 0, sizeof(photo_data));

    int photo_num = 0;
    char photo_name[64];
    int take_photo = 0;
    int camera_mode = 0;
    int exit_flag = 0;
    for (;;) {
        /* 读取摄像头数据 */
        if (video_dev.vi_ops->get_frame(&video_dev, &video_frame) < 0) {
            ulog_error("%s get frame failed!\n", video_dev.name);
            break;
        }

        /* 选择相机功能 */
        switch (mouse_val) {
        case MOUSE_LEFT_RELEASE:
            /* 拍照功能 */
            if (camera_mode == 0) {
                take_photo = 1;
                ulog_info("take_photo\n");
            }
            break;
        case MOUSE_RIGHT_RELEASE:
            /* 切换相册和显示功能 */
            camera_mode ^= 0x1;
            ulog_info("camera_mode:%d\n", camera_mode);
            if (camera_mode & 0x1) {
                photo_get_currentdata(ph_handle, &photo_data.data,
                    &photo_data.total_length);
            }
            break;
        case MOUSE_MIDDLE_RELEASE:
            /* 切换摄像头 */
            // exit_flag = 1;
            if (camera_mode == 0) {
                if (++video_index >= video_num)
                    video_index = 0;
                ulog_debug("video_index:%d\n", video_index);

                /* 取出存在队列中的一个缓冲区 */
                video_dev.vi_ops->put_frame(&video_dev, &video_frame);

                video_dev = video_devs[video_index];
                video_dev.vi_ops->start_dev(&video_dev);

                if (video_dev.pixel_format == V4L2_PIX_FMT_MJPEG)
                    img_handle = imghdl_get_handler("mjpeg2rgb");
                else if(video_dev.pixel_format == V4L2_PIX_FMT_YUYV)
                    img_handle = imghdl_get_handler("yuv2rgb");

                mouse_val = MOUSE_UNKNOW;
                continue;
            }
            break;
        case MOUSE_WHEEL_UP:
            if (camera_mode & 0x1) {
                /* 进入相册功能才生效 */
                ulog_info("photo_lgetdata\n");
                if (photo_lgetdata(ph_handle, &photo_data.data, 
                            &photo_data.total_length) < 0) {
                    ulog_warn("photo_lgetdata failed!\n");
                }
            }
            break;
        case MOUSE_WHEEL_DOWN:
            if (camera_mode & 0x1) {
                /* 进入相册功能才生效 */
                ulog_info("photo_rgetdata\n");
                if (photo_rgetdata(ph_handle, &photo_data.data, 
                            &photo_data.total_length) < 0) {
                    ulog_warn("photo_rgetdata failed!\n");
                }
            }
            break;
        default:
            break;
        }
        mouse_val = MOUSE_UNKNOW;

        if (exit_flag)
            break;

        if (take_photo) {
            take_photo = 0;
            memset(photo_name, 0, sizeof(photo_name));
            if (video_frame.pixelformat == V4L2_PIX_FMT_MJPEG) {
                snprintf(photo_name, sizeof(photo_name), 
                        "%s%d.jpg", PHOTO_NAME, photo_num++);
            } else if(video_frame.pixelformat == V4L2_PIX_FMT_YUYV) {
                snprintf(photo_name, sizeof(photo_name), 
                        "%s%d.yuv", PHOTO_NAME, photo_num++);
            }
            if (photo_append(ph_handle, photo_name, 
                            video_frame.data, video_frame.total_length) < 0) {
                ulog_warn("photo_append faled\n");
            }
        }

        if (camera_mode & 0x1) {
            /* 相册模式 */
            if (photo_data.data) {
                raw_frame.data = photo_data.data;
                raw_frame.total_length = photo_data.total_length;
            } else {
                /* 无数据显示黑屏 */
                disp_dev.ops->clean_screen(&disp_dev, 0xffffff);
                video_dev.vi_ops->put_frame(&video_dev, &video_frame);
                continue;
            }
        } else {
            /* 显示模式 */
            raw_frame = video_frame;
        }

        /* jpeg/yuyv转rgb */
        // ulog_debug("call convert\n");
        if (img_handle->convert(&convert_data, &raw_frame) < 0) {
            ulog_error("%s convert failed\n", img_handle->name);
            break;
        }
        lcd_data = &convert_data;
  
        /* 如果图像分辨率大于LCD, 缩放 */
        if (convert_data.width > lcd_width || convert_data.height > lcd_height) {
            zoom_data.width = lcd_width;
            zoom_data.height = lcd_height;
            zoom_data.bpp = lcd_bpp;
            zoom_data.line_length = zoom_data.width * (zoom_data.bpp >> 3);
            zoom_data.total_length = zoom_data.line_length * zoom_data.height;
            if (zoom_data.data == NULL) {
                zoom_data.data = malloc(zoom_data.total_length);
                if (!zoom_data.data) {
                    ulog_warn("malloc faled\n");
                    break;
                }
            }

            /* 开始缩放图像 */
            if (imghdl_zoom(&convert_data, &zoom_data) < 0)
                break;

            lcd_data = &zoom_data;
        }

        disp_dev.ops->show_screen(&disp_dev, lcd_data);

        video_dev.vi_ops->put_frame(&video_dev, &video_frame);
    }
    if (zoom_data.data)
        free(zoom_data.data);

    disp_dev.ops->remove(&disp_dev);
	// video_dev.vi_ops->remove_dev(&video_dev);
    vctl_destroy_devices(&video_devs);
    img_handle->convert_free(&convert_data);
	photo_destroy(ph_handle);
    usbmouse_close(mouse_fd);

    pthread_cancel(tid);
    pthread_join(tid, NULL);

	return 0;
}
