#include <stdio.h>
#include "display_manager.h"
#include "ulog.h"


int disp_register_ops(struct display_device *dev, struct display_ops *ops)
{
    if (dev == NULL || ops == NULL) 
        return -1;
    
    dev->ops = ops;
    ulog_info("display_ops: %s register success\n", dev->ops->name);

    return 0;
}