#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "display_manager.h"
#include "display_framebuffer.h"
#include "ulog.h"

int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("Usage: %s </dev/fbX>\n", argv[0]);
        return -1;
    }

    struct display_device disp_dev;

    if (dispfb_register(&disp_dev) < 0)
        return -1;

    int ret = disp_dev.ops->init(argv[1], &disp_dev);
    if (ret < 0) {

        return -1;
    }

    struct frame_datas disp_data = {
        .width = disp_dev.x_res,
        .height = disp_dev.y_res,
        .bpp = disp_dev.bpp,
        .line_length = disp_dev.linelength,
        .total_length = disp_dev.linelength * disp_dev.y_res,
        .data = NULL,
    };

    struct frame_datas red_data = disp_data;
    unsigned int red = 0xff0000;
    unsigned int green = 0x00ff00;
    unsigned int blue = 0x0000ff;
#if 1
    int x, y;
    for (y=0; y < disp_dev.y_res; y++) {
        for (x = 0; x < disp_dev.x_res; x++) {
            disp_dev.ops->show_pixel(&disp_dev, x, y, red);
        }
    }
    sleep(1);

    for (y=0; y < disp_dev.y_res; y++) 
        for (x = 0; x < disp_dev.x_res; x++) {
            disp_dev.ops->show_pixel(&disp_dev, x, y, green);
        }
    sleep(1);

    for (y=0; y < disp_dev.y_res; y++) 
        for (x = 0; x < disp_dev.x_res; x++) {
            disp_dev.ops->show_pixel(&disp_dev, x, y, blue);
        }
    sleep(1);
#else
    red_data.data = malloc(red_data.total_length);
    memset(red_data.data, red, red_data.total_length);

    disp_dev.ops->show_screen(&disp_dev, &red_data);

#endif
    disp_dev.ops->remove(&disp_dev);

    return 0;
}