#ifndef _DISPLAY_MANAGER_H
#define _DISPLAY_MANAGER_H

struct display_memory;
struct display_ops;

// enum framebuffer_type {
//     FB_MMAP_BUFFER = 0,
// };

struct display_device {

    int x_res;
    int y_res;
    int bpp;
    int linelength;
    unsigned char *base_addr;    /* 显存基地址 */

    // struct display_memory *dp_mem;

    struct display_ops *ops;
};

// struct display_memory {
//     int id;
 
// };

struct display_ops {
    char *name;
    int (*init)(const char *path, struct display_device *dev);
    int (*remove)(struct display_device *dev);
    int (*show_pixel)(struct display_device *dev, int x, int y, 
                        unsigned int color);
    int (*show_screen)(struct display_device *dev, void *data);
    int (*clean_screen)(struct display_device *dev, unsigned int color);
};

int disp_register_ops(struct display_device *dev, struct display_ops *ops);

#endif /* _DISPLAY_MANAGER_H */