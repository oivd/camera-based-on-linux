#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <errno.h>

/*显示屏相关头文件*/
#include <linux/fb.h>
#include <sys/mman.h>

#include "display_framebuffer.h"
#include "ulog.h"

static int g_fd;

static int dispfb_init(const char *fb_path, struct display_device *dev)
{
    struct fb_var_screeninfo fb_var;
    struct fb_fix_screeninfo fb_fix;

    int fd = open(fb_path, O_RDWR);
    if (fd < 0) {
        ulog_warn("open failed:%s\n", strerror(errno));
        return -1;
    }

    /* get var screeninfo */
    int ret = ioctl(fd, FBIOGET_VSCREENINFO, &fb_var);
    if (ret < 0) {
        ulog_warn("ioctl FBIOGET_VSCREENINFO failed:%s\n", strerror(errno));
        goto ioc_err;
    }

    /* get fix screeninfo */
    ret = ioctl(fd, FBIOGET_FSCREENINFO, &fb_fix);
    if (ret < 0) {
        ulog_warn("ioctl FBIOGET_FSCREENINFO failed:%s\n", strerror(errno));
        goto ioc_err;
    }

    ulog_debug("==========LCD info==========\n");
    ulog_debug("LCD id:%s\n", fb_fix.id);
    ulog_debug("w*h: %d*%d\n", fb_var.xres, fb_var.yres);
    ulog_debug("bpp: %d\n", fb_var.bits_per_pixel);
    ulog_debug("smem_len: %d\n", fb_fix.smem_len);
    ulog_debug("line_length: %d\n", fb_fix.line_length);
    ulog_debug("rgb format: RGB%d%d%d\n", fb_var.red.length, 
                                    fb_var.green.length, 
                                    fb_var.blue.length);
    ulog_debug("============================\n");

    int screen_size = fb_fix.line_length * fb_var.yres;

    unsigned char *fb_base = mmap(0, screen_size, PROT_READ | PROT_WRITE, 
                                MAP_SHARED, fd, 0);
    if ((void *)fb_base == MAP_FAILED) {
       ulog_warn("mmap failed:%s\n", strerror(errno));
       goto mmap_err;
    }

    g_fd = fd;
    dev->x_res = fb_var.xres;
    dev->y_res = fb_var.yres;
    dev->bpp = fb_var.bits_per_pixel;
    dev->linelength = fb_fix.line_length;
    dev->base_addr = fb_base;

    return 0;
mmap_err:
ioc_err:
    close(fd);
    return ret;
}

static int dispfb_remove(struct display_device *dev)
{

    close(g_fd);
    munmap(dev->base_addr, dev->linelength*dev->y_res);

    return 0;
}

static int 
dispfb_show_pixel(struct display_device *dev, int x, int y, unsigned int color)
{
    if (x > dev->x_res || y > dev->y_res) {
        ulog_error("out of region\n");
        return -1;
    }

    unsigned char *base_mem = dev->base_addr +  y * dev->linelength + 
                              x * (dev->bpp >> 3);
	unsigned short *p16base_mem = (unsigned short *)base_mem;
	unsigned int *p32base_mem = (unsigned int *)base_mem;

    int red;
    int green;
    int blue;

    switch (dev->bpp) {
    case 8:
        *base_mem = (unsigned char)(color & 0xf);
        break;

    case 16:
        red   = (color >> 16) & 0xff;
        green = (color >> 8) & 0xff;
        blue  = (color >> 0) & 0xff;
        *p16base_mem  = (unsigned short )(((red >> 3) << 11) | 
                                        ((green >> 2) << 5) | 
                                        ((blue >> 3) << 0));
        break;

    case 32:
        *p32base_mem = color;
        break;
    
    default:
        ulog_warn("can not support %d bpp\n", dev->bpp);
        return -1;
    }

    return 0;
}

// static int dispfb_show_screen(struct display_device *dev, void *data)
// {
//     return 0;
// }

static int 
dispfb_show_screen_fromvideo(struct display_device *dev, void *data)
{
    struct frame_datas *frm_data = (struct frame_datas *)data;

    int min_y = (dev->y_res < frm_data->height) ? dev->y_res : 
                                                  frm_data->height;
    // int min_x = (dev->x_res < frm_data->width) ? dev->x_res : 
    //                                               frm_data->width;
    // int min_linelen = (dev->linelength < frm_data->line_length) ?
    //                     dev->linelength : frm_data->line_length; 

    // ulog_debug("devy:%d, devx:%d, dev_linelen:%d\n", dev->y_res, dev->x_res, dev->linelength);
    // ulog_debug("frm_size:%d*%d, frm_linelen:%d\n", frm_data->width, 
    //                                                frm_data->height, 
    //                                                frm_data->line_length);                            
    // ulog_debug("miny:%d, minx:%d, min_linelen:%d\n", frm_data->height, frm_data->width, frm_data->line_length);
    
    int y = 0;
    unsigned char *dev_addr = dev->base_addr;
    unsigned char *data_addr = frm_data->data;
    /* 当LCD的水平分辨率 大于 数据水平分辨率 */
    if (dev->x_res > frm_data->width) {
        for (y = 0; y < min_y; y++) {
            memcpy(dev_addr, data_addr, frm_data->line_length);

            dev_addr += dev->linelength ;
            data_addr += frm_data->line_length;
        }
    } else {
        int dev_total = dev->linelength * dev->y_res;
        int min_totallen = (dev_total < frm_data->total_length) ? dev_total : 
                                                       frm_data->total_length;
        memcpy(dev->base_addr, frm_data->data, min_totallen);
    }

    return 0;
}

static int dispfb_clear_screen(struct display_device *dev, unsigned int color)
{
    return 0;
}

static struct display_ops fb_ops = {
    .name = "fb_ops",
    .init = dispfb_init,
    .remove = dispfb_remove,
    .show_pixel = dispfb_show_pixel,
    .show_screen = dispfb_show_screen_fromvideo,
    .clean_screen = dispfb_clear_screen,
};

int dispfb_register(struct display_device *dev)
{
    return disp_register_ops(dev, &fb_ops);
}