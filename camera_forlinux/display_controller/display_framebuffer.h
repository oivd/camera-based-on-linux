#ifndef _DISPLAY_FRAMEBUFFER_H
#define _DISPLAY_FRAMEBUFFER_H

#include "display_manager.h"
#include "../video_controller/video_controller.h"

int dispfb_register(struct display_device *dev);

#endif /* _DISPLAY_FRAMEBUFFER_H */