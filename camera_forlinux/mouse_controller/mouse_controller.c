#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/input.h>
#include <time.h>

#include "mouse_controller.h"
#include "ulog.h"

int usbmouse_open(void)
{
    int max = 16;
    int fd, i;
    char event_name[25];
    char buf[256];
    for (i = 0; i < max; i++) {
        sprintf(event_name, "/dev/input/event%d", i);
        fd = open(event_name, O_RDONLY);
        if (fd < 0 && errno == ENOENT)
            break;
        else if (fd < 0)
            continue;
        else {
            memset(&buf, 0, sizeof(buf));
            if (ioctl(fd, EVIOCGNAME(sizeof(buf)), buf) < 0) {
                fd = -1;
                continue;
            }
            if (strcasestr(buf, "mouse") == NULL) {
                close(fd);
                continue;
            } else {
                ulog_debug("event opened:%s\n", event_name);
                break;
            }
        }
    }

    return fd;
}

void usbmouse_close(int m_fd)
{
    close(m_fd);
}

int usbmouse_get_val(int m_fd)
{
    struct input_event event;

    int ret = read(m_fd, &event, sizeof(event));
    if (ret < 0)
        return -1;
    
    // ulog_debug(("%-24.24s.%06lu type 0x%04x; code 0x%04x;
    //                    value 0x%08x\n",
    //                    ctime(&event.time.tv_sec),
    //                    event.time.tv_usec,
    //                    event.type, event.code, event.value));
    switch (event.type) {
    case EV_KEY:
        switch (event.code) {
            case BTN_LEFT: 
                if (!event.value) return MOUSE_LEFT_RELEASE;
            case BTN_RIGHT: 
                if (!event.value) return MOUSE_RIGHT_RELEASE;
            case BTN_MIDDLE: 
                if (!event.value) return MOUSE_MIDDLE_RELEASE;
        }
        break;
    case EV_REL:
        switch (event.code) {
            case REL_WHEEL:
                if (event.value == 0x01) 
                    return MOUSE_WHEEL_UP;
                else if (event.value == 0xff) 
                    return MOUSE_WHEEL_DOWN;
            break;
        }
        break;

    default:
        break;
    }

    return MOUSE_UNKNOW;
}