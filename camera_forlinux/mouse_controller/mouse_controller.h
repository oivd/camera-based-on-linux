#ifndef _MOUSE_CONTROLLER_H
#define _MOUSE_CONTROLLER_H

#define MOUSE_UNKNOW            0
#define MOUSE_LEFT_PRESS        1
#define MOUSE_LEFT_RELEASE      2
#define MOUSE_MIDDLE_PRESS      3
#define MOUSE_MIDDLE_RELEASE    4
#define MOUSE_RIGHT_PRESS       5
#define MOUSE_RIGHT_RELEASE     6
#define MOUSE_WHEEL_UP          7
#define MOUSE_WHEEL_DOWN        8


int usbmouse_open(void);

void usbmouse_close(int m_fd);

int usbmouse_get_val(int m_fd);

#endif /* _MOUSE_CONTROLLER_H */