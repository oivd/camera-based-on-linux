#include <linux/types.h>          /* for videodev2.h */
#include <linux/videodev2.h>
#include <stdlib.h>
#include <stdio.h>

#include "image_handler.h"
#include "ulog.h"

static int supported_pixelformat[] = {
    V4L2_PIX_FMT_RGB565,
    V4L2_PIX_FMT_RGB24,
};

static int yuv2rgb_is_support(int dest_pixelformat, int src_pixelformat)
{
    if (src_pixelformat != V4L2_PIX_FMT_YUYV)
        return 0;
    
    int i;
    int arr_len = sizeof(supported_pixelformat) / 
                  sizeof(supported_pixelformat[0]);
    for (i = 0; i < arr_len; i++) {
        if (dest_pixelformat == supported_pixelformat[i])
            return 1;
    }

    return 0;
}


void yuyv_to_rgb565(uint8_t* yuyv, uint16_t* rgb, int width, int height) {
    int i, j;
    uint8_t* p = yuyv;
    uint16_t* q = rgb;
    int y0, u, y1, v;
    int r, g, b;

    for (j = 0; j < height; j++) {
        for (i = 0; i < width; i+=2) {
            y0 = *p++;
            u = *p++;
            y1 = *p++;
            v = *p++;

            r = y0 + ((360 * (v - 128)) >> 8);
            g = y0 - (((88 * (u - 128) + 184 * (v - 128))) >> 8);
            b = y0 + ((455 * (u - 128)) >> 8);

            r = r > 255 ? 255 : r < 0 ? 0 : r;
            g = g > 255 ? 255 : g < 0 ? 0 : g;
            b = b > 255 ? 255 : b < 0 ? 0 : b;

            *q++ = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);

            r = y1 + ((360 * (v - 128)) >> 8);
            g = y1 - (((88 * (u - 128) + 184 * (v - 128))) >> 8);
            b = y1 + ((455 * (u - 128)) >> 8);

            r = r > 255 ? 255 : r < 0 ? 0 : r;
            g = g > 255 ? 255 : g < 0 ? 0 : g;
            b = b > 255 ? 255 : b < 0 ? 0 : b;

            *q++ = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);
        }
    }
}

void yuyv_to_rgb888(uint8_t* yuyv, uint8_t* rgb, int width, int height) {
    int i, j;
    uint8_t* p = yuyv;
    uint8_t* q = rgb;
    int y0, u, y1, v;
    int r, g, b;

    for (j = 0; j < height; j++) {
        for (i = 0; i < width; i+=2) {
            y0 = *p++;
            u = *p++;
            y1 = *p++;
            v = *p++;

            r = y0 + ((360 * (v - 128)) >> 8);
            g = y0 - (((88 * (u - 128) + 184 * (v - 128))) >> 8);
            b = y0 + ((455 * (u - 128)) >> 8);

            r = r > 255 ? 255 : r < 0 ? 0 : r;
            g = g > 255 ? 255 : g < 0 ? 0 : g;
            b = b > 255 ? 255 : b < 0 ? 0 : b;

            *q++ = r;
            *q++ = g;
            *q++ = b;

            r = y1 + ((360 * (v - 128)) >> 8);
            g = y1 - (((88 * (u - 128) + 184 * (v - 128))) >> 8);
            b = y1 + ((455 * (u - 128)) >> 8);

            r = r > 255 ? 255 : r < 0 ? 0 : r;
            g = g > 255 ? 255 : g < 0 ? 0 : g;
            b = b > 255 ? 255 : b < 0 ? 0 : b;

            *q++ = r;
            *q++ = g;
            *q++ = b;
        }
    }
}


static int yuv2rgb_convert(struct frame_datas *dest, struct frame_datas *src)
{
    if (src->pixelformat != V4L2_PIX_FMT_YUYV)
        return -1;

    if (dest->bpp <= 0) {
        ulog_warn("dest bpp Can't be less than or equal to 0, bpp:%d\n",
                    dest->bpp);
        return -1;
    }
    
    if (dest->pixelformat == V4L2_PIX_FMT_RGB565) {
        dest->bpp = 16;
    } else if(dest->pixelformat == V4L2_PIX_FMT_RGB32) {
        dest->bpp = 32;
    }

    dest->width = src->width;
    dest->height = src->height;
    dest->line_length = dest->width * (dest->bpp >> 3);
    dest->total_length = dest->line_length * dest->width;

    if (!dest->data) {
        dest->data = malloc(dest->total_length);
        if (dest->data == NULL) {
            ulog_warn("malloc failed\n");
            return -1;
        }
    }
#if 0
    if (dest->pixelformat == V4L2_PIX_FMT_RGB565 || dest->bpp == 16)
        yuyv_to_rgb565(src->data, dest->data, dest->width, dest->height);
    else if(dest->pixelformat == V4L2_PIX_FMT_RGB32 || dest->bpp == 32)
        yuyv_to_rgb888(src->data, dest->data, dest->width, dest->height);
#else
    yuyv_to_rgb888(src->data, dest->data, dest->width, dest->height);
#endif

    return 0;
}

int yuv2rgb_convert_free(struct frame_datas *dest)
{
    if (dest->data) {
        free(dest->data);
        dest->data = NULL;
    }

    return 0;
}


static struct image_handler yuv2rgb_handle = {
    .name = "yuv2rgb",
    .is_support = yuv2rgb_is_support,
    .convert = yuv2rgb_convert,
    .convert_free = yuv2rgb_convert_free,
};

int yuv2rgb_init(void)
{
    return imghdl_add_handler(&yuv2rgb_handle);
}