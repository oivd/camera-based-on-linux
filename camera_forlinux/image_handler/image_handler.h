#ifndef _IMAGE_HANDLER_H
#define _IMAGE_HANDLER_H

#include "video_controller.h"

struct image_handler {
    char *name;
    int (*is_support)(int dest_pixelformat, int src_pixelformat);
    int (*convert)(struct frame_datas *dest, struct frame_datas *src);
    int (*convert_free)(struct frame_datas *dest);
    struct image_handler *pnext;
};

int imghdl_add_handler(struct image_handler *hdl);

struct image_handler *imghdl_get_handler(const char *name);

struct image_handler *imghdl_get_handler_fromfmt(int dest_pixelformat, 
                                                 int src_pixelformat);

void imghdl_show_handler(void);

int imghdl_zoom(struct frame_datas *origin, struct frame_datas *zoom);

/*  */
int mjpeg2rgb_init(void);
int yuv2rgb_init(void);

void imghdl_init(void);

#endif /* _IMAGE_HANDLER_H */