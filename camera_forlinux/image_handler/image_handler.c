#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "image_handler.h"
#include "ulog.h"

static struct image_handler *g_image_handle = NULL;

void imghdl_init(void)
{
    mjpeg2rgb_init();
    yuv2rgb_init();
}


int imghdl_add_handler(struct image_handler *hdl)
{
    if (hdl == NULL)
        return -1;

    struct image_handler *ptemp;

    if (!g_image_handle) {
        g_image_handle = hdl;
        hdl->pnext = NULL;
    } else {
        ptemp = g_image_handle;
        while (ptemp->pnext) {
            ptemp = ptemp->pnext;
        }

        ptemp->pnext = hdl;
        hdl->pnext = NULL;
    }
    ulog_info("%s add success\n", hdl->name);

    return 0;
}


struct image_handler *imghdl_get_handler(const char *name)
{
    if (name == NULL)
        return NULL;

    struct image_handler *ptemp = g_image_handle;
    
    while (ptemp) {
        if (strcmp(ptemp->name, name) == 0) {
            ulog_info("selected %s\n", ptemp->name);
            return ptemp;
        }
        
        ptemp = ptemp->pnext;
    }

    return NULL;
}

struct image_handler *imghdl_get_handler_fromfmt(int dest_pixelformat, 
                                                 int src_pixelformat)
{
    struct image_handler *ptemp = g_image_handle;

    while (ptemp) {
        if (ptemp->is_support == NULL) {
            ulog_warn("is_support did not realize\n");
            return NULL;
        }

        if (ptemp->is_support(dest_pixelformat, src_pixelformat)) {
            ulog_info("selected %s\n", ptemp->name);
            return ptemp;
        }
        
        ptemp = ptemp->pnext;
    }

    return NULL;
}


void imghdl_show_handler(void)
{
    return ;
}

int imghdl_zoom(struct frame_datas *origin, struct frame_datas *zoom)
{
    int dstwidth = zoom->width;
    int pixel_byte = origin->bpp / 8;
    int x, y;
    unsigned long* psrc_xtable;

    if (origin->bpp != zoom->bpp)
        return -1;
    
    psrc_xtable = malloc(sizeof(unsigned long) * dstwidth);
    if (psrc_xtable == NULL) {
        ulog_warn("malloc failed\n");
        return -1;
    }

    // ulog_debug("zoom start\n");
    ulog_debug("src: %d*%d, %d bpp\n", origin->width, origin->height, origin->bpp);
    ulog_debug("dest: %d*%d, %d bpp\n", zoom->width, zoom->height, zoom->bpp);

    for (x = 0; x < dstwidth; x++) {
        /* 生成表 pdwSrcXTable */
        psrc_xtable[x] = (origin->width * x) / zoom->width;
    }

    unsigned long src_y;
    unsigned char *pdest, *psrc;
    for (y = 0; y < zoom->height; y++) {
        src_y = (y * origin->height) / zoom->height;

        pdest = zoom->data + y * zoom->line_length;
        psrc = origin->data + src_y * origin->line_length;

        for (x = 0; x < dstwidth; x++) {
            /* 原图座标: pdwSrcXTable[x]，srcy
             * 缩放座标: x, y
			 */
            memcpy(pdest + x * pixel_byte, 
                   psrc + psrc_xtable[x] * pixel_byte, 
                   pixel_byte);
        }
    }

    free(psrc_xtable);
    return 0;
}