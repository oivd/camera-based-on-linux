#include <linux/types.h>          /* for videodev2.h */
#include <linux/videodev2.h>
#include <stdlib.h>
#include <stdio.h>

#include "jpeglib.h"
#include "image_handler.h"
#include "ulog.h"

struct bgr888_color {
    unsigned char red;
    unsigned char green;
    unsigned char blue;
};

static int supported_pixelformat[] = {
    V4L2_PIX_FMT_RGB565,
    V4L2_PIX_FMT_RGB24,
};

static inline void 
bgr888torgb565(unsigned short *dest_line, struct bgr888_color *src_line)
{
    *dest_line = ((src_line->red & 0xf8) << 8) | 
                 ((src_line->green & 0xfc) << 3) |
                 ((src_line->blue & 0xf8) >> 3);
}

static int mjpeg2rgb_is_support(int dest_pixelformat, int src_pixelformat)
{
    if (src_pixelformat != V4L2_PIX_FMT_MJPEG)
        return 0;
    
    int i;
    int arr_len = sizeof(supported_pixelformat) / 
                  sizeof(supported_pixelformat[0]);
    for (i = 0; i < arr_len; i++) {
        if (dest_pixelformat == supported_pixelformat[i])
            return 1;
    }

    return 0;
}

static int 
mjpeg2rgb_convert_oneline(int iWidth, int iSrcBpp, int iDstBpp, 
                          unsigned char *pudSrcDatas,
                          unsigned char *pudDstDatas)
{
	unsigned int dwRed;
	unsigned int dwGreen;
	unsigned int dwBlue;
	unsigned int dwColor;

	unsigned short *pwDstDatas16bpp = (unsigned short *)pudDstDatas;
	unsigned int   *pwDstDatas32bpp = (unsigned int *)pudDstDatas;

	int i;
	int pos = 0;

	if (iSrcBpp != 24) {
		return -1;
	}

	if (iDstBpp == 24) {
		memcpy(pudDstDatas, pudSrcDatas, iWidth*3);
	}
	else {
		for (i = 0; i < iWidth; i++) {
			dwRed   = pudSrcDatas[pos++];
			dwGreen = pudSrcDatas[pos++];
			dwBlue  = pudSrcDatas[pos++];
			if (iDstBpp == 32) {
				dwColor = (dwRed << 16) | (dwGreen << 8) | dwBlue;
				*pwDstDatas32bpp = dwColor;
				pwDstDatas32bpp++;
			} else if (iDstBpp == 16) {
				/* 565 */
				dwRed   = dwRed >> 3;
				dwGreen = dwGreen >> 2;
				dwBlue  = dwBlue >> 3;
				dwColor = (dwRed << 11) | (dwGreen << 5) | (dwBlue);
				*pwDstDatas16bpp = dwColor;
				pwDstDatas16bpp++;
			}
		}
	}
	return 0;
}

static int mjpeg2rgb_convert(struct frame_datas *dest, struct frame_datas *src)
{
    if (src->pixelformat != V4L2_PIX_FMT_MJPEG)
        return -1;

    if (dest->bpp <= 0) {
        ulog_warn("dest bpp Can't be less than or equal to 0, bpp:%d\n",
                    dest->bpp);
        return -1;
    }

    struct jpeg_decompress_struct dec_info;
    struct jpeg_error_mgr jerr;
    // int ret;

    /* 绑定默认错误处理函数 */
    dec_info.err = jpeg_std_error(&jerr);

    /* 创建JPEG解码对象 */
    jpeg_create_decompress(&dec_info);

    /* 指定数据 */
    jpeg_mem_src(&dec_info, src->data, src->total_length);

    /* 读取图像信息 */
    jpeg_read_header(&dec_info, TRUE);
    // ulog_debug("jpeg size: %d*%d\n", dec_info.image_width, dec_info.image_height);

    /* 设置解码参数,比如放大、缩小 */
    dec_info.out_color_space = JCS_RGB; //默认就是JCS_RGB
    //cinfo.scale_num = 1;
    //cinfo.scale_denom = 2;

    /* 开始解码图像 */
    jpeg_start_decompress(&dec_info);

    // ulog_debug("===================jpeg info=================\n");
    // ulog_debug("jpeg outsize: %d*%d\n", dec_info.output_width, dec_info.output_height);
    // ulog_debug("jpeg output_components:%d\n", dec_info.output_components);
    // ulog_debug("jpeg output_scanline:%d\n", dec_info.output_scanline);
    // ulog_debug("=============================================\n");
    /* 获取已解码数据的信息, 判断图像和LCD屏那个的分辨率更低*/
    // int min_w = (dest->width < dec_info.output_width) ? dest->width : 
    //                                          dec_info.output_width;
    // int min_h = (dest->height < dec_info.output_height) ? dest->height : 
    //                                          dec_info.output_height;

    dest->width = dec_info.output_width;
    dest->height = dec_info.output_height;
    // dest->bpp = 24;
    dest->line_length = dec_info.output_width * (dest->bpp >> 3);
    dest->total_length = dec_info.output_height * dest->line_length;

    unsigned char *jpeg_line_buf = malloc(dec_info.output_components * 
                                                dec_info.output_width);
    if (jpeg_line_buf == NULL) {
        ulog_warn("malloc failed\n");
        goto alloc_err;
    }
    
    if (dest->data == NULL) {
        dest->data = malloc(dest->total_length);
        if (dest->data == NULL) {
            ulog_warn("malloc failed\n");
            goto alloc_err1;
        }
    }

    /* 读取数据 */
    unsigned char *pdest = dest->data;
    while (dec_info.output_scanline < dec_info.output_height) {
        jpeg_read_scanlines(&dec_info, &jpeg_line_buf, 1);

        // bgr888torgb565(temp, jpeg_line_buf);
        mjpeg2rgb_convert_oneline(dest->width, 24, dest->bpp, jpeg_line_buf, pdest);

        pdest += dest->line_length;
    }

    /* 完成解码 */
    jpeg_finish_decompress(&dec_info); //完成解码
    jpeg_destroy_decompress(&dec_info);//销毁JPEG解码对象、释放资源

    free(jpeg_line_buf);

    return 0;
alloc_err1:
    free(jpeg_line_buf);
alloc_err:
    jpeg_finish_decompress(&dec_info);
    jpeg_destroy_decompress(&dec_info);
    return -1;
}

int mjpeg2rgb_convert_free(struct frame_datas *dest)
{
    if (dest->data) {
        free(dest->data);
        dest->data = NULL;
    }

    return 0;
}


static struct image_handler mjpeg2rgb_handle = {
    .name = "mjpeg2rgb",
    .is_support = mjpeg2rgb_is_support,
    .convert = mjpeg2rgb_convert,
    .convert_free = mjpeg2rgb_convert_free,
};


int mjpeg2rgb_init(void)
{
    return imghdl_add_handler(&mjpeg2rgb_handle);
}