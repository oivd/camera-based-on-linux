#include <stdio.h>
#include "video_controller.h"



int main(int argc, char **argv)
{
	if (argc != 2) {
        printf("Usage: %s </dev/videoX>\n", argv[0]);
        return -1;
    }

	struct video_device video_dev;
	struct frame_datas frame;

	int ret = vctl_register_ops(&video_dev, NULL);
	if (ret < 0) {
		
		return -1;
	}

	video_dev.vi_ops->init_dev(argv[1], &video_dev);

	video_dev.vi_ops->start_dev(&video_dev);

	char file_name[20];
	FILE *fp;
	for (int i = 0; i < 10; i++) {
		video_dev.vi_ops->get_frame(&video_dev, &frame);
		
		sprintf(file_name, "image%d.jpg", i);   /* 构造文件名 */
		// sprintf(file_name, "image%d.yuv", i);   /* 构造文件名 */
        fp = fopen(file_name, "w+");
        if (!fp) {
            perror("fopen error");
            goto err;
        }

		fwrite(frame.data, frame.total_length, 1, fp);   /* 将图像数据写入文件 */
        fclose(fp);

		video_dev.vi_ops->put_frame(&video_dev, &frame);
	}

err:
	video_dev.vi_ops->remove_dev(&video_dev);
	
	return 0;
}
