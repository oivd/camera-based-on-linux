#ifndef _VIDEO_CONTROLLER_H
#define _VIDEO_CONTROLLER_H

#define VCTL_BUFFER_COUNT 5

struct video_device {
    char *name;

    int fd;
    int pixel_format;
    int width;
    int height;

    int bufcnt;
    int buf_maxlen;
    int buf_index;
    unsigned char *data_buf[VCTL_BUFFER_COUNT];

    struct video_ops *vi_ops;
};

struct frame_datas {
    int pixelformat;
    int width;
    int height;
    int bpp;
    int line_length;
    long total_length;
    unsigned char *data;
};

struct video_ops {
    char *name;
    int (*init_dev)(const char *devpath, struct video_device *dev);
    int (*remove_dev)(struct video_device *dev);
    // int (*get_format)(struct video_device *dev);
    int (*set_format)(struct video_device *dev);
    int (*get_frame)(struct video_device *dev, struct frame_datas *frm);
    int (*put_frame)(struct video_device *dev, struct frame_datas *frm);
    int (*start_dev)(struct video_device *dev);
    int (*stop_dev)(struct video_device *dev);
};

int vctl_register_ops(struct video_device *dev, struct video_ops *ops);

int vctl_init_devices(struct video_device **devs);

void vctl_destroy_devices(struct video_device **devs);

#endif /* _VIDEO_CONTROLLER_H */
