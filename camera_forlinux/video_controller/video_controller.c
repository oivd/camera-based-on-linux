#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <linux/types.h>          /* for videodev2.h */
#include <linux/videodev2.h>
#include <errno.h>
#include <sys/mman.h>

#include "video_controller.h"
#include "ulog.h"

#define DEV_NUMBER_MAX 10

static unsigned int g_supported_format[] = {
    V4L2_PIX_FMT_MJPEG,
    V4L2_PIX_FMT_YUYV,
};

/* 偷一下懒,直接用全局变量每个存储摄像头信息 */
static struct video_device g_devices[DEV_NUMBER_MAX];
static int used_num = 0;

static int issupport_format(unsigned int format)
{
    int i;
    int array_len = sizeof(g_supported_format) / sizeof(g_supported_format[0]);
    for (i = 0; i < array_len; i++) {
        if (g_supported_format[i] == format)
            return 1;
    }

    return 0;
}

static int vctl_init_dev(const char *devpath, struct video_device *dev)
{
    int ret = 0;

    if (dev == NULL)
        return -1;

    /* 打开设备节点 */
    dev->fd = open(devpath, O_RDWR);
    if (dev->fd < 0) {
        ulog_warn("open failed:%s\n", strerror(errno));
        return -errno;
    }

    /* 获取设备能力 */
    struct v4l2_capability cap;

    memset(&cap, 0, sizeof(struct v4l2_capability));
    ret = ioctl(dev->fd, VIDIOC_QUERYCAP, &cap);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_QUERYCAP failed:%s\n", strerror(errno));
        goto ioc_err;
    }
    dev->name = strdup(cap.card);

    /* 检测能力 */
    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        ulog_warn("%s is not a video capture device\n", cap.card);
        ret = -1;
        goto ioc_err;
    }

    if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
        ulog_warn("%s not support read/write io\n", cap.card);
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        ulog_warn("%s not support stream io\n", cap.card);
    }

    /* 枚举设备格式 */
    struct v4l2_fmtdesc fmtdesc;

    memset(&fmtdesc, 0, sizeof(struct v4l2_fmtdesc));
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmtdesc.index = 0;
    for (;;) {
        ret = ioctl(dev->fd, VIDIOC_ENUM_FMT, &fmtdesc);
        if (ret < 0) {
            if (errno != EINVAL) {
                ulog_warn("ioctl VIDIOC_ENUM_FMT failed:%s\n", strerror(errno));
                goto ioc_err;
            } else {
                break;
            }
        }

        if (issupport_format(fmtdesc.pixelformat)) {
            dev->pixel_format = fmtdesc.pixelformat;
            break;
        }
        fmtdesc.index++;
    }

    if (!dev->pixel_format) {
        ulog_warn("No format was found that supports this device\n");
        ret = -1;
        goto ioc_err;
    }

    /* 设置设备的图像格式 */
    struct v4l2_format format;
    memset(&format, 0, sizeof(struct v4l2_format));

    /* 
     * 如果设置的分辨率不支持，驱动程序会对其进行修改，
     * 将其设置为与应用设置值最接近的设备支持的分辨率 
     */
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = 640;
    format.fmt.pix.height = 480;
    format.fmt.pix.pixelformat = dev->pixel_format;
    format.fmt.pix.field = V4L2_FIELD_ANY;

    ret = ioctl(dev->fd, VIDIOC_S_FMT, &format);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_S_FMT failed:%s\n", strerror(errno));
        goto ioc_err;
    }
    dev->width = format.fmt.pix.width;
    dev->height = format.fmt.pix.height;

    /* 申请缓冲区 request buffers */
    struct v4l2_requestbuffers reqbuffers;
    memset(&reqbuffers, 0 ,sizeof(struct v4l2_requestbuffers));
    reqbuffers.count = VCTL_BUFFER_COUNT;
    reqbuffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbuffers.memory = V4L2_MEMORY_MMAP;

    ret = ioctl(dev->fd, VIDIOC_REQBUFS, &reqbuffers);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_REQBUFS failed:%s\n", strerror(errno));
        goto ioc_err;
    }
    dev->bufcnt = reqbuffers.count;
    
    /* 将内核态缓冲区映射到用户空间 */
    struct v4l2_buffer buffer;
    int i;
    for (i = 0; i < dev->bufcnt; i++) {
        memset(&buffer, 0, sizeof(struct v4l2_buffer));
        buffer.index = i;
        buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buffer.memory = V4L2_MEMORY_MMAP;
        ret = ioctl(dev->fd, VIDIOC_QUERYBUF, &buffer);
        if (ret < 0) {
            ulog_warn("ioctl VIDIOC_QUERYBUF failed:%s\n", strerror(errno));
            goto ioc_err;
        }

        /* 映射mmap类型的缓冲区内存 */
        if (dev->buf_maxlen < buffer.length)
            dev->buf_maxlen = buffer.length;
        dev->data_buf[i] = mmap(NULL, buffer.length, PROT_READ, 
                            MAP_SHARED, dev->fd, buffer.m.offset);
        if ((void *)dev->data_buf[i] == MAP_FAILED) {
            ulog_warn("mmap failed:%s\n", strerror(errno));
            ret = -1;
            goto map_err;
        }
    }

    /* 将缓冲区加入queued队列 */
    for (i = 0; i < dev->bufcnt; i++) {
        memset(&buffer, 0, sizeof(struct v4l2_buffer));
        buffer.index = i;
        buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buffer.memory = V4L2_MEMORY_MMAP;

        ret = ioctl(dev->fd, VIDIOC_QBUF, &buffer);
        if (ret < 0) {
            ulog_warn("ioctl VIDIOC_QBUF failed:%s\n", strerror(errno));
            goto map_err;
        }
    }

    return 0;
map_err:
    for (i = 0; i < dev->bufcnt; i++) {
        munmap(dev->data_buf[i], dev->buf_maxlen);
    }
ioc_err:
    close(dev->fd);
    return ret;
}

static int vctl_remove_dev(struct video_device *dev)
{
    if (dev == NULL)
        return -1;

    dev->vi_ops->stop_dev(dev);
    
    int i;
    for (i = 0; i < dev->bufcnt; i++) {
        if (dev->data_buf[i]) {
            munmap(dev->data_buf[i], dev->buf_maxlen);
            dev->data_buf[i] = NULL;
        }
    }

    free(dev->name);
    close(dev->fd);
    return 0;
}

int vctl_start_streaming(struct video_device *dev)
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    int ret = ioctl(dev->fd, VIDIOC_STREAMON, &type);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_STREAMON failed:%s\n", strerror(errno));
        return -1;
    }
    ulog_debug("%s start success\n", dev->name);

    return 0;
}

int 
vctl_getframe_formstreaming(struct video_device *dev, struct frame_datas *frm)
{
    if (dev == NULL || frm == NULL)
        return -1;

    struct v4l2_buffer buffer;
    int ret;

    memset(&buffer, 0, sizeof(struct v4l2_buffer));
    /* 缓冲区出队 */
    buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buffer.memory = V4L2_MEMORY_MMAP;
    ret = ioctl(dev->fd, VIDIOC_DQBUF, &buffer);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_DQBUF failed:%s\n", strerror(errno));
        return -1;
    }
    ulog_debug("%s v4l2_buffer index:%d dequeue success\n", 
                dev->name, buffer.index);

    dev->buf_index = buffer.index;

    frm->pixelformat = dev->pixel_format;
    frm->width = dev->width;
    frm->height = dev->height;
    frm->bpp = 0;
    frm->line_length = dev->width * (frm->bpp >> 3);
    frm->total_length = buffer.bytesused;
    frm->data = dev->data_buf[buffer.index];

    return 0;
}

int 
vctl_putframe_formstreaming(struct video_device *dev, struct frame_datas *frm)
{
    if (dev == NULL)
        return -1;

    struct v4l2_buffer buffer;
    int ret;

    /* 将缓冲区加入queued队列 */
    memset(&buffer, 0, sizeof(struct v4l2_buffer));
    buffer.index = dev->buf_index;
    buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buffer.memory = V4L2_MEMORY_MMAP;
    ret = ioctl(dev->fd, VIDIOC_QBUF, &buffer);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_QBUF failed:%s\n", strerror(errno));
        return -1;
    }
    ulog_debug("%s v4l2_buffer index:%d queue success\n", 
                dev->name, buffer.index);

    return 0;
}

int vctl_stop_streaming(struct video_device *dev)
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    int ret = ioctl(dev->fd, VIDIOC_STREAMOFF, &type);
    if (ret < 0) {
        ulog_warn("ioctl VIDIOC_STREAMOFF failed:%s\n", strerror(errno));
        return -1;
    }
    ulog_debug("%s stop success\n", dev->name);

    return 0;
}

static struct video_ops default_ops = {
    .name = "v4l2_ops",
    .init_dev = vctl_init_dev,
    .remove_dev = vctl_remove_dev,
    .get_frame = vctl_getframe_formstreaming,
    .put_frame = vctl_putframe_formstreaming,
    .start_dev = vctl_start_streaming,
    .stop_dev = vctl_stop_streaming,
    .set_format = NULL,
};

int vctl_register_ops(struct video_device *dev, struct video_ops *ops)
{
    if (ops == NULL) {
        dev->vi_ops = &default_ops;
        return 0;
    } else {
        dev->vi_ops = ops;
    }
    ulog_info("video_ops: %s register success\n", dev->vi_ops->name);

    return 0;
}

int vctl_init_devices(struct video_device **devs)
{
    if (devs == NULL)
        return -1;

    *devs = &g_devices;

    int fd, i;
    int ret;
    char video_path[25];
    for (i = 0; i < DEV_NUMBER_MAX; i++) {
        sprintf(video_path, "/dev/video%d", i);

        memset(&g_devices[used_num], 0, sizeof(struct video_device));
        
        vctl_register_ops(&g_devices[used_num], NULL);
        ret = g_devices[used_num].vi_ops->init_dev(video_path,
                                        &g_devices[used_num]);
        if (ret < 0 && ret == -ENOENT) {
            break;
        } else if(ret < 0) {
            continue;
        }

        ++used_num;
    }

    return used_num;
}

void vctl_destroy_devices(struct video_device **devs)
{
    int i;

    for (i = 0; i < used_num; i++) {
        g_devices[i].vi_ops->remove_dev(&g_devices[i]);
    }
    *devs = NULL;
}

#if 0
void print_capability(void)
{
    printf("========== uvc_camera capability ==========\n");
    printf("driver = %s\n", cap.driver);
    printf("card = %s\n", cap.card);
    printf("bus_info = %s\n", cap.bus_info);
    printf("version = %u.%u.%u\n", (cap.version >> 16) & 0xFF, 
                                    (cap.version >> 8) & 0xFF, 
                                    cap.version & 0xFF);
    printf("capabilities = 0x%x\n", cap.capabilities);
    printf("device_caps = 0x%x\n", cap.device_caps);
    printf("===========================================\n");
}
#endif