#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/hid.h>
 
#define DBG_PRINT(fmt, ...) \
    printk("<%s:%d> "fmt"",__FUNCTION__, __LINE__, ##__VA_ARGS__)

struct my_usbmouse {
    char *data;
    struct urb *urb;
    struct input_dev *input;
    dma_addr_t phy_dma;
    int maxp;
};

// static struct my_usbmouse my_mouse;
 
//定义USB的IDTAB
// static const struct usb_device_id my_usb_ids[] = {
//     {USB_DEVICE(0x093a,0x2510)},
//     {USB_DEVICE(0x0c45,0x760b)},
//     {}
// };
 
/* 通用ID 通用的usb设备id可以写成如下写法。就可以识别所有的USB鼠标 */
static const struct usb_device_id my_usb_ids[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID, USB_INTERFACE_SUBCLASS_BOOT,
		USB_INTERFACE_PROTOCOL_MOUSE) },
	{ }	/* Terminating entry */
};


/*
MODULE_DEVICE_TABLE 有两个功能。
一是：将设备加入到外设队列中,
二是告诉程序阅读者该设备是热插拔设备或是说该设备支持热插拔功能。
该宏定义在<linux/module.h>下
这个宏有两个参数，第一个参数设备名，
第二个参数该设备加入到模块中时对应产生的设备搜索符号，
这个宏生成了一个名为__mod_pci_device_table
局部变量，这个变量指向第二个参数
*/
MODULE_DEVICE_TABLE (usb,my_usb_ids);


static void usb_irq(struct urb *urb)
{
    struct my_usbmouse *mouse = urb->context;
    struct input_dev *dev = mouse->input;
    char *data = mouse->data;
#if 0
    int i;
    DBG_PRINT("");
    for (i = 0; i < mouse->maxp; i++) {
        printk("0x%x ", data[i]);
    }
    printk("\n");

    if (data[0] & 0x01)
		DBG_PRINT("left btn down\n");
	if (data[0] & 0x02)
		DBG_PRINT("right btn down\n");
	if (data[0] & 0x04)
		DBG_PRINT("mid btn down\n");
	if (data[0] & 0x08)
		DBG_PRINT("side btn down\n");
	if (data[0] & 0x10)
		DBG_PRINT("extra btn down\n");
 
    if (data[1] || data[2])
	    DBG_PRINT("rel x:[%d], rely:[%d]\n",data[1],data[2]);
    
    if (data[3])
	    DBG_PRINT("wheel rel:[%d]\n", data[3]);
#endif
    /* 提交按键信息，data[0] 的第 0 位为 1，表示左键按下
     * 提交按键信息，data[0] 的第 1 位为 1，表示右键按下
     * 提交按键信息，data[0] 的第 2 位为 1，表示中键按下
     * 提交按键信息，data[0] 的第 3 位为 1，表示边键按下
     * 提交按键信息，data[0] 的第 4 位为 1，表示额外键按下 
     */
    input_report_key(dev, BTN_LEFT,   data[0] & 0x01);
    input_report_key(dev, BTN_RIGHT,  data[0] & 0x02);
    input_report_key(dev, BTN_MIDDLE, data[0] & 0x04);
    input_report_key(dev, BTN_SIDE,   data[0] & 0x08);
    input_report_key(dev, BTN_EXTRA,  data[0] & 0x10);
    
    /* 提交鼠标相对坐标值，data[1] 为 X 坐标
     * 提交鼠标相对坐标值，data[2] 为 Y 坐标 
     * 提交鼠标滚轮相对值，data[3] 为 滚轮相对值
     */
    input_report_rel(dev, REL_X,     data[1]);
    input_report_rel(dev, REL_Y,     data[2]);
    input_report_rel(dev, REL_WHEEL, data[3]);

    input_sync(dev);

    /* 重新提交异步请求*/
	usb_submit_urb(mouse->urb, GFP_ATOMIC);
}

static int myusb_open(struct input_dev *dev)
{
    struct my_usbmouse *mouse = input_get_drvdata(dev);

    if (usb_submit_urb(mouse->urb, GFP_KERNEL))
		return -EIO;

    return 0;
}

static void myusb_close(struct input_dev *dev)
{
    struct my_usbmouse *mouse = input_get_drvdata(dev);

    usb_kill_urb(mouse->urb);
}
 
//USB设备信息与驱动端匹配成功的时候调用。
static int 
myusb_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
    struct usb_device *dev = NULL;
	struct usb_host_interface *interface = NULL;
	struct usb_endpoint_descriptor *endpoint = NULL;
    struct my_usbmouse *mouse;
    struct input_dev *input_dev;
	int pipe, maxsize, err;

	printk("%s %d\n",__FUNCTION__, __LINE__);
    DBG_PRINT("usb driver match successfully: iVendor=0x%x,iProduct:0x%x\n",
                id->idVendor, id->idProduct);

    /* 通过接口获取USB设备信息 */
    dev = interface_to_usbdev(intf);

    /* 通过接口获取设备信息 */
    interface = intf->cur_altsetting;

    /* 通过接口获取设备信息 */
    if (interface->desc.bNumEndpoints != 1)
		return -ENODEV;

    endpoint = &interface->endpoint[0].desc;
	if (!usb_endpoint_is_int_in(endpoint))
		return -ENODEV; 

    /* 中断传输:创建输入管道 */
	pipe = usb_rcvintpipe(dev, endpoint->bEndpointAddress);

    /* 从端点描述符中获取传输的数据大小 */
	// maxsize = endpoint->wMaxPacketSize;
    maxsize = usb_maxpacket(dev, pipe, usb_pipeout(pipe));
    DBG_PRINT("endpoint wMaxPacketSize:%d\n", maxsize);

    /* my_usbmouse结构体申请空间 */
    mouse = kzalloc(sizeof(struct my_usbmouse), GFP_KERNEL);
    mouse->maxp = maxsize;

    /* 分配数据传输缓冲区 */
	mouse->data = usb_alloc_coherent(dev, maxsize, GFP_ATOMIC,
                                        &mouse->phy_dma);

    /* 分配新的urb,urb是usb设备驱动中用来描述与usb设备通信
     * 所用的基本载体和核心数据结构 */
	mouse->urb = usb_alloc_urb(0, GFP_KERNEL);

    /* 中断方式初始化urb */
	usb_fill_int_urb(mouse->urb, dev, pipe, mouse->data, maxsize, 
                     usb_irq, mouse, endpoint->bInterval);
	mouse->urb->transfer_dma = mouse->phy_dma;
	mouse->urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

    /* 为端点提交异步传输请求 */
    /* 移动到usb_open中提交传输请求 */
	// usb_submit_urb(mouse->urb, GFP_KERNEL);

    /* alloc input_dev */
    input_dev = devm_input_allocate_device(&intf->dev);

    mouse->input = input_dev;
    input_set_drvdata(input_dev, mouse);
    
    /* setting input_dev */
#if 1
    __set_bit(EV_KEY, input_dev->evbit);
    __set_bit(EV_REL, input_dev->evbit);

    __set_bit(BTN_LEFT, input_dev->keybit);
    __set_bit(BTN_RIGHT, input_dev->keybit);
    __set_bit(BTN_MIDDLE, input_dev->keybit);

    __set_bit(REL_X, input_dev->relbit);
    __set_bit(REL_Y, input_dev->relbit);
    __set_bit(REL_WHEEL, input_dev->relbit);
#else 
    input_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REL);
	input_dev->keybit[BIT_WORD(BTN_MOUSE)] = BIT_MASK(BTN_LEFT) |
		BIT_MASK(BTN_RIGHT) | BIT_MASK(BTN_MIDDLE);
	input_dev->relbit[0] = BIT_MASK(REL_X) | BIT_MASK(REL_Y);
	input_dev->keybit[BIT_WORD(BTN_MOUSE)] |= BIT_MASK(BTN_SIDE) |
		BIT_MASK(BTN_EXTRA);
	input_dev->relbit[0] |= BIT_MASK(REL_WHEEL);
#endif

    input_dev->name = "myusbmouse";

    /* open/close */
    input_dev->open = myusb_open;
    input_dev->close = myusb_close;

    /* register input_dev */
    err = input_register_device(input_dev);
    if (err < 0) {
        DBG_PRINT("input_register_device err\n");
    }

    usb_set_intfdata(intf, mouse);

    return 0;
}
 
//USB断开的时候调用
static void myusb_disconnect(struct usb_interface *intf)
{
    struct my_usbmouse *mouse = usb_get_intfdata(intf);
	printk("%s %d\n",__FUNCTION__, __LINE__);

    usb_set_intfdata(intf, NULL);

    if (mouse) {
        usb_kill_urb(mouse->urb);
        input_unregister_device(mouse->input);
        usb_free_urb(mouse->urb);
        usb_free_coherent(interface_to_usbdev(intf), mouse->maxp, mouse->data,
                                             mouse->phy_dma);
        kfree(mouse);
    }
    DBG_PRINT("usb mouse free successfully\n");
}
 
//定义USB驱动结构体 
static struct usb_driver myusb_driver = {
    .name = "myusb_mouse",
    .probe = myusb_probe,
    .disconnect = myusb_disconnect,
    .id_table = my_usb_ids,
};
 
static int __init myusb_init(void)
{
    //注册USB设备驱动
	printk("%s %d\n",__FUNCTION__, __LINE__);
    return usb_register(&myusb_driver);;
}
 
static void __exit myusb_exit(void)
{
     //注销USB设备驱动
	printk("%s %d\n",__FUNCTION__, __LINE__);
    usb_deregister(&myusb_driver);
}

module_init(myusb_init);
module_exit(myusb_exit);
MODULE_AUTHOR("96oivd");
MODULE_LICENSE("GPL");

