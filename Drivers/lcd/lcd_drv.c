#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/fb.h>
#include <linux/clk.h>
#include <video/display_timing.h>
#include <video/of_display_timing.h>
#include <linux/gpio/consumer.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>

#define DBG_PRINT(fmt, ...) \
    printk("<%s:%d> "fmt"",__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define RED 0
#define GREEN 1
#define BLUE 2
#define TRANSP 3

/** LCDIF - Register Layout Typedef */
struct imx6ull_elcdif{
  volatile unsigned int CTRL;                              /**< eLCDIF General Control Register, offset: 0x0 */
  volatile unsigned int CTRL_SET;                          /**< eLCDIF General Control Register, offset: 0x4 */
  volatile unsigned int CTRL_CLR;                          /**< eLCDIF General Control Register, offset: 0x8 */
  volatile unsigned int CTRL_TOG;                          /**< eLCDIF General Control Register, offset: 0xC */
  volatile unsigned int CTRL1;                             /**< eLCDIF General Control1 Register, offset: 0x10 */
  volatile unsigned int CTRL1_SET;                         /**< eLCDIF General Control1 Register, offset: 0x14 */
  volatile unsigned int CTRL1_CLR;                         /**< eLCDIF General Control1 Register, offset: 0x18 */
  volatile unsigned int CTRL1_TOG;                         /**< eLCDIF General Control1 Register, offset: 0x1C */
  volatile unsigned int CTRL2;                             /**< eLCDIF General Control2 Register, offset: 0x20 */
  volatile unsigned int CTRL2_SET;                         /**< eLCDIF General Control2 Register, offset: 0x24 */
  volatile unsigned int CTRL2_CLR;                         /**< eLCDIF General Control2 Register, offset: 0x28 */
  volatile unsigned int CTRL2_TOG;                         /**< eLCDIF General Control2 Register, offset: 0x2C */
  volatile unsigned int TRANSFER_COUNT;                    /**< eLCDIF Horizontal and Vertical Valid Data Count Register, offset: 0x30 */
       unsigned char RESERVED_0[12];
  volatile unsigned int CUR_BUF;                           /**< LCD Interface Current Buffer Address Register, offset: 0x40 */
       unsigned char RESERVED_1[12];
  volatile unsigned int NEXT_BUF;                          /**< LCD Interface Next Buffer Address Register, offset: 0x50 */
       unsigned char RESERVED_2[12];
  volatile unsigned int TIMING;                            /**< LCD Interface Timing Register, offset: 0x60 */
       unsigned char RESERVED_3[12];
  volatile unsigned int VDCTRL0;                           /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register0, offset: 0x70 */
  volatile unsigned int VDCTRL0_SET;                       /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register0, offset: 0x74 */
  volatile unsigned int VDCTRL0_CLR;                       /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register0, offset: 0x78 */
  volatile unsigned int VDCTRL0_TOG;                       /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register0, offset: 0x7C */
  volatile unsigned int VDCTRL1;                           /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register1, offset: 0x80 */
       unsigned char RESERVED_4[12];
  volatile unsigned int VDCTRL2;                           /**< LCDIF VSYNC Mode and Dotclk Mode Control Register2, offset: 0x90 */
       unsigned char RESERVED_5[12];
  volatile unsigned int VDCTRL3;                           /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register3, offset: 0xA0 */
       unsigned char RESERVED_6[12];
  volatile unsigned int VDCTRL4;                           /**< eLCDIF VSYNC Mode and Dotclk Mode Control Register4, offset: 0xB0 */
       unsigned char RESERVED_7[12];
  volatile unsigned int DVICTRL0;                          /**< Digital Video Interface Control0 Register, offset: 0xC0 */
       unsigned char RESERVED_8[12];
  volatile unsigned int DVICTRL1;                          /**< Digital Video Interface Control1 Register, offset: 0xD0 */
       unsigned char RESERVED_9[12];
  volatile unsigned int DVICTRL2;                          /**< Digital Video Interface Control2 Register, offset: 0xE0 */
       unsigned char RESERVED_10[12];
  volatile unsigned int DVICTRL3;                          /**< Digital Video Interface Control3 Register, offset: 0xF0 */
       unsigned char RESERVED_11[12];
  volatile unsigned int DVICTRL4;                          /**< Digital Video Interface Control4 Register, offset: 0x100 */
       unsigned char RESERVED_12[12];
  volatile unsigned int CSC_COEFF0;                        /**< RGB to YCbCr 4:2:2 CSC Coefficient0 Register, offset: 0x110 */
       unsigned char RESERVED_13[12];
  volatile unsigned int CSC_COEFF1;                        /**< RGB to YCbCr 4:2:2 CSC Coefficient1 Register, offset: 0x120 */
       unsigned char RESERVED_14[12];
  volatile unsigned int CSC_COEFF2;                        /**< RGB to YCbCr 4:2:2 CSC Coefficent2 Register, offset: 0x130 */
       unsigned char RESERVED_15[12];
  volatile unsigned int CSC_COEFF3;                        /**< RGB to YCbCr 4:2:2 CSC Coefficient3 Register, offset: 0x140 */
       unsigned char RESERVED_16[12];
  volatile unsigned int CSC_COEFF4;                        /**< RGB to YCbCr 4:2:2 CSC Coefficient4 Register, offset: 0x150 */
       unsigned char RESERVED_17[12];
  volatile unsigned int CSC_OFFSET;                        /**< RGB to YCbCr 4:2:2 CSC Offset Register, offset: 0x160 */
       unsigned char RESERVED_18[12];
  volatile unsigned int CSC_LIMIT;                         /**< RGB to YCbCr 4:2:2 CSC Limit Register, offset: 0x170 */
       unsigned char RESERVED_19[12];
  volatile unsigned int DATA;                              /**< LCD Interface Data Register, offset: 0x180 */
       unsigned char RESERVED_20[12];
  volatile unsigned int BM_ERROR_STAT;                     /**< Bus Master Error Status Register, offset: 0x190 */
       unsigned char RESERVED_21[12];
  volatile unsigned int CRC_STAT;                          /**< CRC Status Register, offset: 0x1A0 */
       unsigned char RESERVED_22[12];
  volatile const   unsigned int STAT;                              /**< LCD Interface Status Register, offset: 0x1B0 */
       unsigned char RESERVED_23[76];
  volatile unsigned int THRES;                             /**< eLCDIF Threshold Register, offset: 0x200 */
       unsigned char RESERVED_24[12];
  volatile unsigned int AS_CTRL;                           /**< eLCDIF AS Buffer Control Register, offset: 0x210 */
       unsigned char RESERVED_25[12];
  volatile unsigned int AS_BUF;                            /**< Alpha Surface Buffer Pointer, offset: 0x220 */
       unsigned char RESERVED_26[12];
  volatile unsigned int AS_NEXT_BUF;                       /**< , offset: 0x230 */
       unsigned char RESERVED_27[12];
  volatile unsigned int AS_CLRKEYLOW;                      /**< eLCDIF Overlay Color Key Low, offset: 0x240 */
       unsigned char RESERVED_28[12];
  volatile unsigned int AS_CLRKEYHIGH;                     /**< eLCDIF Overlay Color Key High, offset: 0x250 */
       unsigned char RESERVED_29[12];
  volatile unsigned int SYNC_DELAY;                        /**< LCD working insync mode with CSI for VSYNC delay, offset: 0x260 */
} ;

static const struct fb_bitfield def_rgb888[] = {
	[RED] = {
		.offset = 16,
		.length = 8,
	},
	[GREEN] = {
		.offset = 8,
		.length = 8,
	},
	[BLUE] = {
		.offset = 0,
		.length = 8,
	},
	[TRANSP] = {	/* no support for transparency */
		.length = 0,
	}
};

static inline unsigned chan_to_field(unsigned chan, struct fb_bitfield *bf)
{
	chan &= 0xffff;
	chan >>= 16 - bf->length;
	return chan << bf->offset;
}

static int myfb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
		u_int transp, struct fb_info *fb_info)
{
	unsigned int val;
	int ret = -EINVAL;

    // DBG_PRINT("setcol: regno=%d, rgb=%d,%d,%d\n",
	// 	   regno, red, green, blue);

	/*
	 * If greyscale is true, then we convert the RGB value
	 * to greyscale no matter what visual we are using.
	 */
	if (fb_info->var.grayscale)
		red = green = blue = (19595 * red + 38470 * green +
					7471 * blue) >> 16;

	switch (fb_info->fix.visual) {
	case FB_VISUAL_TRUECOLOR:
		/*
		 * 12 or 16-bit True Colour.  We encode the RGB value
		 * according to the RGB bitfield information.
		 */
		if (regno < 16) {
			u32 *pal = fb_info->pseudo_palette;

			val  = chan_to_field(red, &fb_info->var.red);
			val |= chan_to_field(green, &fb_info->var.green);
			val |= chan_to_field(blue, &fb_info->var.blue);

			pal[regno] = val;
			ret = 0;
		}
		break;

	case FB_VISUAL_STATIC_PSEUDOCOLOR:
	case FB_VISUAL_PSEUDOCOLOR:
		break;
	}

	return ret;
}

static struct fb_ops lcdfb_ops = {
	.owner		= THIS_MODULE,
    .fb_setcolreg = myfb_setcolreg,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
};

static inline void imx6ull_elcdif_enable(struct imx6ull_elcdif *elcdif)
{
	elcdif->CTRL |= (1<<0);
}

static inline void imx6ull_elcdif_disable(struct imx6ull_elcdif *elcdif)
{
	elcdif->CTRL &= ~(1<<0);
}

static int lcd_controller_init(struct imx6ull_elcdif *elcdif, 
                                struct fb_info *info, 
                                struct display_timing *dt, 
                                unsigned int bus_width)
{
    unsigned int input_data_format;
	unsigned int data_bus_width;
	unsigned int hsync_active = 0;
	unsigned int vsync_active = 0;
	unsigned int de_active = 0;
	unsigned int pixelclk_active = 0;

    if (info->var.bits_per_pixel == 16)
		input_data_format = 0x0;
	else if (info->var.bits_per_pixel == 8)
		input_data_format = 0x1;
	else if (info->var.bits_per_pixel == 18)
		input_data_format = 0x2;
	else if (info->var.bits_per_pixel == 24 || info->var.bits_per_pixel == 32)
		input_data_format = 0x3;
	else {
		printk("Don't support %d bpp\n", info->var.bits_per_pixel);
		return -1;
	}

    if (bus_width == 16)
		data_bus_width = 0x0;
	else if (bus_width == 8)
		data_bus_width = 0x01;
	else if (bus_width == 18)
		data_bus_width = 0x02;
	else if (bus_width == 24)
		data_bus_width = 0x03;
	else {
		printk("Don't support %d bit data bus mode\n", bus_width);
		return -1;		
	}

    /* elcdif正常运行bit31、bit30  必须设置为0 */
	elcdif->CTRL &= ~((1 << 31) | (1 << 30)); 

    /* 
     * 初始化LCD控制器的CTRL寄存器
     * [19]       :  1      : DOTCLK和DVI modes需要设置为1 
     * [17]       :  1      : 设置为1工作在DOTCLK模式
     * [15:14]    : 00      : 输入数据不交换（小端模式）默认就为0，不需设置
     * [13:12]    : 00      : CSC数据不交换（小端模式）默认就为0，不需设置
     * [11:10]    : 11		: 数据总线为24bit
     * [9:8]    根据显示屏资源文件bpp来设置：8位0x1 ， 16位0x0 ，24位0x3
     * [5]        :  1      : 设置elcdif工作在主机模式
     * [1]        :  0      : 24位数据均是有效数据，默认就为0，不需设置
	 */
    /* 设置RGB格式和数据宽度 */
	elcdif->CTRL &= ~((0x03 << 8) | (0x03 << 10));
	elcdif->CTRL |= ((input_data_format << 8) | (data_bus_width << 10));

    elcdif->CTRL |= (1 << 17); /* 选择 RGB 模式 */
    elcdif->CTRL |= (1 << 19); /* 选择 RGB 模式 开启显示 */
    elcdif->CTRL |= (1 << 5);  /* 设置elcdf接口为主模式 */

    /*
	* 设置ELCDIF的寄存器CTRL1
	* 根据bpp设置，bpp为24或32才设置
	* [19:16]  : 111  :表示ARGB传输格式模式下，传输24位无压缩数据，A通道不用传输）
	*/
    if (info->var.bits_per_pixel == 24 || info->var.bits_per_pixel == 32) {
        /* 设置32位有效位的低24位有效 */
		elcdif->CTRL1 &= ~(0xf << 16);
		elcdif->CTRL1 |= (0x7 << 16);
    } else {
        elcdif->CTRL1 |= (0xf << 16);
    }

    /*
	* 设置ELCDIF的寄存器TRANSFER_COUNT寄存器
	* [31:16]  : 垂直方向上的像素个数  
	* [15:0]   : 水平方向上的像素个数
	*/
    /* 设置LCD分辨率 */
	elcdif->TRANSFER_COUNT &= ~0xffffffff;
	elcdif->TRANSFER_COUNT |= (dt->vactive.typ << 16); /* 设置LCD垂直分辨率 */
	elcdif->TRANSFER_COUNT |= (dt->hactive.typ << 0);  /* 设置LCD水平分辨率 */

    /*
	* 设置ELCDIF的VDCTRL0寄存器
	* [29] 0 : VSYNC输出  ，默认为0，无需设置
	* [28] 1 : 在DOTCLK模式下，设置1硬件会产生使能ENABLE输出
	* [27] 0 : VSYNC低电平有效	,根据屏幕配置文件将其设置为0
	* [26] 0 : HSYNC低电平有效 , 根据屏幕配置文件将其设置为0
	* [25] 1 : DOTCLK下降沿有效 ，根据屏幕配置文件将其设置为1
	* [24] 1 : ENABLE信号高电平有效，根据屏幕配置文件将其设置为1
	* [21] 1 : 帧同步周期单位，DOTCLK mode设置为1
	* [20] 1 : 帧同步脉冲宽度单位，DOTCLK mode设置为1
	* [17:0] :  vysnc脉冲宽度 
	*/
    elcdif->VDCTRL0 |= (1 << 28);  /* 生成使能信号 */
    elcdif->VDCTRL0 |= (1 << 21);  /* 设置VSYNC周期 的单位为显示时钟的时钟周期 */
    elcdif->VDCTRL0 |= (1 << 20);  /* 设置VSYNC 脉冲宽度的单位为显示时钟的时钟周期 */
    
    if (dt->flags & DISPLAY_FLAGS_DE_HIGH)
		de_active = 1;
	if (dt->flags & DISPLAY_FLAGS_PIXDATA_POSEDGE)
		pixelclk_active = 1;
	if (dt->flags & DISPLAY_FLAGS_HSYNC_HIGH)
		hsync_active = 1;
	if (dt->flags & DISPLAY_FLAGS_VSYNC_HIGH)
		vsync_active = 1;

    /* 设置信号极性 */
    // elcdif->VDCTRL0 |= (1 << 28);
	elcdif->VDCTRL0 &= ~(0xf << 24);            /* bit24~bit27清零 */
	elcdif->VDCTRL0 |= (de_active << 24);       /* 设置数据使能信号的有效电平 */
	elcdif->VDCTRL0 |= (pixelclk_active << 25); /* 设置时钟信号极性 */
	elcdif->VDCTRL0 |= (hsync_active << 26);    /* 设置HSYNC有效电平 */
	elcdif->VDCTRL0 |= (vsync_active << 27);    /* 设置VSYNC有效电平 */

	elcdif->VDCTRL0 |= (dt->vsync_len.typ << 0);/* 设置vysnc脉冲宽度 */

    /*
	* 设置ELCDIF的VDCTRL1寄存器
	* 设置垂直方向的总周期:上黑框tvb+垂直同步脉冲tvp+垂直有效高度yres+下黑框tvf
	*/
    /* 设置VSYNC信号周期 */
   	elcdif->VDCTRL1 = dt->vsync_len.typ + dt->vactive.typ + 
                      dt->vfront_porch.typ + dt->vback_porch.typ; 

    /*
	* 设置ELCDIF的VDCTRL2寄存器
	* [18:31]  : 水平同步信号脉冲宽度
	* [17: 0]   : 水平方向总周期
	* 设置水平方向的总周期:左黑框thb+水平同步脉冲thp+水平有效高度xres+右黑框thf
	*/ 
    /*设置HSYNC信号周期 */
    elcdif->VDCTRL2 = (dt->hsync_len.typ << 18) | 
                    (dt->hback_porch.typ + dt->hsync_len.typ + 
                     dt->hactive.typ + dt->hfront_porch.typ);

    /*
	* 设置ELCDIF的VDCTRL3寄存器
	* [27:16] ：水平方向上的等待时钟数 = thb + thp
	* [15:0]  : 垂直方向上的等待时钟数 = tvb + tvp
	*/ 
    elcdif->VDCTRL3 = ((dt->hback_porch.typ + dt->hsync_len.typ) << 16) | 
                    (dt->vback_porch.typ + dt->vsync_len.typ);

    /*
	* 设置ELCDIF的VDCTRL4寄存器
	* [18]	   使用VSHYNC、HSYNC、DOTCLK模式此为置1
	* [17:0]  : 水平方向的宽度
	*/ 
    elcdif->VDCTRL4 = (1<<18) | (dt->hactive.typ << 0);

    /*
	* 设置ELCDIF的CUR_BUF和NEXT_BUF寄存器
	* CUR_BUF	 :	当前显存地址
	* NEXT_BUF :	下一帧显存地址
	* 方便运算，都设置为同一个显存地址
	*/ 
   	elcdif->CUR_BUF  =  info->fix.smem_start;
	elcdif->NEXT_BUF =  info->fix.smem_start;

    return 0;
}

static int lcd_driver_probe(struct platform_device *pdev)
{
    struct resource *res = NULL;
    struct imx6ull_elcdif *elcdif;
    struct fb_info *lcdfb_info;
    struct device_node *display_np;
    struct display_timings *timings = NULL;
	struct display_timing  *dt = NULL;
    unsigned int bits_per_pixel;
	unsigned int bus_width;
    struct clk *clk_pix;
    struct clk *clk_axi;
    struct clk *disp_axi;

    display_np = of_parse_phandle(pdev->dev.of_node, "display", 0);
    timings = of_get_display_timings(display_np);
    dt = timings->timings[timings->native_mode];

    /* get common info */
    of_property_read_u32(display_np, "bus-width", &bus_width);
    of_property_read_u32(display_np, "bits-per-pixel", &bits_per_pixel);

    /* get clk from device tree */
    clk_pix = devm_clk_get(&pdev->dev, "pix");
	clk_axi = devm_clk_get(&pdev->dev, "axi"); 
    // disp_axi = devm_clk_get(&pdev->dev, "disp_axi"); 

    /* set clk rate */
	clk_set_rate(clk_pix, dt->pixelclock.typ);

    /* enable clk */
	clk_prepare_enable(clk_pix);
	clk_prepare_enable(clk_axi);
    // clk_prepare_enable(disp_axi);

    /* 分配一个fb_info结构体 */
	lcdfb_info = framebuffer_alloc(0, &pdev->dev);
    platform_set_drvdata(pdev, lcdfb_info);

    /* LCD屏幕参数设置 */
	lcdfb_info->var.xres   = dt->hactive.typ;
	lcdfb_info->var.yres   = dt->vactive.typ;
	lcdfb_info->var.width  = dt->hactive.typ;
	lcdfb_info->var.height = dt->vactive.typ;
	lcdfb_info->var.xres_virtual = dt->hactive.typ;
	lcdfb_info->var.yres_virtual = dt->vactive.typ;
	lcdfb_info->var.bits_per_pixel = bits_per_pixel;

    /* LCD信号时序设置 */
	lcdfb_info->var.pixclock  = dt->pixelclock.typ;
	lcdfb_info->var.left_margin	 = dt->hback_porch.typ;
	lcdfb_info->var.right_margin = dt->hfront_porch.typ;
	lcdfb_info->var.upper_margin = dt->vback_porch.typ;
	lcdfb_info->var.lower_margin = dt->vfront_porch.typ;
	lcdfb_info->var.vsync_len = dt->vsync_len.typ;
	lcdfb_info->var.hsync_len = dt->hsync_len.typ;

    /* LCD RGB格式设置, 这里使用的是RGB565 */
    if (lcdfb_info->var.bits_per_pixel == 16) {
        lcdfb_info->var.red.offset   = 11;
        lcdfb_info->var.red.length   = 5;

        lcdfb_info->var.green.offset = 5;
        lcdfb_info->var.green.length = 6;

        lcdfb_info->var.blue.offset  = 0;
        lcdfb_info->var.blue.length  = 5;
    } else if (lcdfb_info->var.bits_per_pixel == 24 ||
                lcdfb_info->var.bits_per_pixel == 32) {
        lcdfb_info->var.red    = def_rgb888[RED];
        lcdfb_info->var.green  = def_rgb888[GREEN];
        lcdfb_info->var.blue   = def_rgb888[BLUE];
        lcdfb_info->var.transp = def_rgb888[TRANSP];
    }


    /* 设置固定参数 */
	strcpy(lcdfb_info->fix.id, "mylcd");
	lcdfb_info->fix.type   = FB_TYPE_PACKED_PIXELS;
	lcdfb_info->fix.visual = FB_VISUAL_TRUECOLOR;
	lcdfb_info->fix.line_length = dt->hactive.typ * 
                                  lcdfb_info->var.bits_per_pixel / 8;                   
	lcdfb_info->fix.smem_len	= dt->hactive.typ * dt->vactive.typ * 
                                  lcdfb_info->var.bits_per_pixel / 8;

    /* 其他参数设置 */
	lcdfb_info->screen_size = dt->hactive.typ * dt->vactive.typ * 
                              bits_per_pixel / 8;

    /* dma_alloc_writecombine：分配smem_len大小的内存，
    * 返回screen_base虚拟地址，对应的物理地址保存在smem_start 
    */
	lcdfb_info->screen_base = dma_alloc_writecombine(&pdev->dev, 
                                lcdfb_info->fix.smem_len, 
                                (dma_addr_t*)&lcdfb_info->fix.smem_start,
                                GFP_KERNEL);
	lcdfb_info->pseudo_palette = devm_kzalloc(&pdev->dev, sizeof(u32) * 16,
					            GFP_KERNEL);
	lcdfb_info->fbops = &lcdfb_ops;

    /* elcdif控制器硬件初始化 */
    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	elcdif = devm_ioremap_resource(&pdev->dev, res);

    if (lcd_controller_init(elcdif, lcdfb_info, dt, bus_width) < 0) {
        DBG_PRINT("lcd_controller_init failed\n");
    }
	imx6ull_elcdif_enable(elcdif);

    /* 注册fb_info结构体 */
	register_framebuffer(lcdfb_info);

    DBG_PRINT("probe success\n");
    return 0;
}

static int lcd_driver_remove(struct platform_device *pdev)
{
    struct fb_info *fb_info = platform_get_drvdata(pdev);

    DBG_PRINT("lcd_driver_remove");

    devm_kfree(&pdev->dev, fb_info->pseudo_palette);
    unregister_framebuffer(fb_info);
    framebuffer_release(fb_info);

    return 0;
}

static struct of_device_id	lcd_of_match[] = {
	{.compatible = "96oivd,lcd_drv",},
	{},
};

static struct platform_driver lcd_driver = {
	.probe  = lcd_driver_probe,
	.remove = lcd_driver_remove,
	.driver = {
		.name = "lcd_drv",
		.of_match_table = lcd_of_match,
	},
};

static int __init lcd_driver_init(void)
{
    DBG_PRINT("lcd_driver_init\n");
	return platform_driver_register(&lcd_driver);
}

static void __exit lcd_driver_exit(void)
{
	platform_driver_unregister(&lcd_driver);
}

module_init(lcd_driver_init);
module_exit(lcd_driver_exit);
MODULE_AUTHOR("96oivd");
MODULE_DESCRIPTION("Framebuffer driver for the linux");
MODULE_LICENSE("GPL");